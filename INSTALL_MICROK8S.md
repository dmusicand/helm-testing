# Install MicroK8s (will install latest stable)
```bash
snap install microk8s --classic
```

# Enable your user to run it
```bash
# The second command will only work if you installed kubectl for that user
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube

# Re-enter the session for the group change to work
su - $USER
```

# Enable addons
```bash
microk8s enable dns storage ingress
```

# Enable Helm
```bash
microk8s.enable helm3
```

# Caveats When Using MicroK8s

# MicroK8s namespaces all clients like Helm and Kubectl

```bash
# Calling kubectl, you *have* to use MicroK8s' version
microk8s kubectl get nodes

# Calling Helm, you *have* to use MicroK8s' version named "helm3"
microk8s helm3 list
```

# Trying the default app created by Helm
After you create and install the Helm default app, you'll need to edit the commands it prints out to you.

## Basic Create/Install
```bash
microk8s helm3 create my-chart
microk8s helm3 install myapp my-chart/
```

It'll then print out NOTES where you'll need to add in the `microk8s` as below:

```bash
export POD_NAME=$(microk8s kubectl get pods --namespace default -l "app.kubernetes.io/name=db-app,app.kubernetes.io/instance=testapp" -o jsonpath="{.items[0].metadata.name}")
echo "Visit http://127.0.0.1:8080 to use your application"
microk8s kubectl --namespace default port-forward $POD_NAME 8080:80
```