# Helm Testing

## SSH Remote
git remote set-url origin git@gitlab.com:dmusicand/helm-testing.git

## Ubuntu

* [EKS](INSTALL_EKS.md)
* [k3d](INSTALL_K3D.md)
* [Minikube](INSTALL_MINIKUBE.md)
* [MicroK8s](INSTALL_MICROK8S.md)

# Schemas

Writing schemas in Json is a lot of work. So we can use a workaround to make this
a quicker exercise:

1. Create a new file `values.schema.json` next to your `values.yaml` file
1. Convert your `values.yaml` file to JSON using [https://www.json2yaml.com](https://www.json2yaml.com/)
1. Paste that generated JSON into [https://www.jsonschema.net](https://www.jsonschema.net/)
   1. click on "Infer Schema"
1. Paste the generated schema into the `values.schema.json` file

## Be Careful!

Using the above method creates a schema, but the rules are likely not correct. In some cases,
we'll get a failure when linting, but in others it'll pass linting but have the wrong
rule.

For example, if you use the default helm app and create a schema for it following our 
above steps, you'll get an error.

```bash
helm lint my-app/
```

**Error:**
> [INFO] Chart.yaml: icon is recommended
> [ERROR] templates/: values don't meet the specifications of the schema(s) in the following chart(s):  
> db-app:  
> \- serviceAccount: name is required

In reality, the `name` property is *not* required. If ommitted, it'll generate a name for you.