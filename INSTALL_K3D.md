- [Install K3d](#install-k3d)
- [K3d vs K3s](#k3d-vs-k3s)
- [Create a Cluster](#create-a-cluster)
  * [Single Node Cluster](#single-node-cluster)
  * [Multi-Node Cluster](#multi-node-cluster)
- [Deleting a Cluster](#deleting-a-cluster)
- [Making the Server Nodes "Control Plane" Only](#making-the-server-nodes-control-plane-only)
  * [Creating Control Plane Servers](#creating-control-plane-servers)
    + [Checking The Taint Was Applied](#checking-the-taint-was-applied)
- [Install Traefik](#install-traefik)
  * [Checking Traefik](#checking-traefik)
- [Adding Worker Nodes](#adding-worker-nodes)
- [Trying Out the Cluster](#trying-out-the-cluster)
  * [Create the Chart](#create-the-chart)
  * [Install Nginx](#install-nginx)
    + [Complete "hooking" it up](#complete--hooking--it-up)
  * [Verifying Node Scheduling](#verifying-node-scheduling)
- [Cleaning Up](#cleaning-up)
  * [Delete the Cluster](#delete-the-cluster)
  * [Clean Up Docker](#clean-up-docker)

# Install K3d

We're using K3d so we can run these on our laptop in Docker.

```bash
wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
```

# K3d vs K3s

You can think of k3d as having two aspects:

1. It installs k3s nodes into docker containers
1. It handles running the many commands needed to hook these nodes all together as well as creating other things like load balancers for you

It simplifies what you need to do to get k3s pieces working together.

# Create a Cluster

## Single Node Cluster

```bash
k3d cluster create dev --api-port 6551 --port "8081:80@loadbalancer"
```

WARNING: If you will want to add more server nodes to your cluster later, you *must* create the cluster with the `--cluster-init` flag.

Command explained:

* `k3d cluster create <CLUSTER_NAME>`
* `--api-port` This is the port for kubectl commands
* `--port "<HOST_PORT>:<CONTAINER_PORT>@<NODE_FILTER>"` You *must* specify a node filter or it'll complain. The one we chose matches the `serverlb` that k3d deploys in front of a cluster's server nodes
  * You can also map multiple ports to that load balancer. e.g. `k3d cluster create devcluster -p 80:80@loadbalancer -p 443:443@loadbalancer`

## Multi-Node Cluster

```bash
# Will create 3 nodes
k3d cluster create dev --api-port 6551 --port "8081:80@loadbalancer" --servers 3
```

You'll now see that it created a load balancer for you and 3 master nodes behind it:

```plaintext
CONTAINER ID    IMAGE      ...elided...      PORTS                                            NAMES
0fd98e4e5fab    rancher/k3d-proxy:v3.4.0     0.0.0.0:8081->80/tcp, 0.0.0.0:6551->6443/tcp     k3d-dev-serverlb
90c730a7335a    rancher/k3s:v1.19.4-k3s1                                                      k3d-dev-server-2
302685edb761    rancher/k3s:v1.19.4-k3s1                                                      k3d-dev-server-1
0c015445fedb    rancher/k3s:v1.19.4-k3s1                                                      k3d-dev-server-0
```

# Deleting a Cluster

```bash
# k3d cluster delete <CLUSTER_NAME>
k3d cluster delete dev
```

# Making the Server Nodes Control Plane Only

The point of having worker nodes is so that we can have our pods live on those and not bog down the kubernetes pods and control plane nodes. To do this, we need to use a new concept `taint` vs `affinity`.

* `Node Affinity`: Something that attracts certain pods to be created on specific nodes
  * An example is `nodeSelector` in your deployment
* `Node Taint`: Keeps certain pods from running on specific nodes
  * An example is setting a node to `NoExecute` which will stop pods from being deployed and run on it

## Creating Control Plane Servers

```bash
k3d cluster create dev --api-port 6551 --port "8081:80@loadbalancer" --servers 3 --k3s-server-arg --node-taint="CriticalAddonsOnly=true:NoExecute" --k3s-server-arg "--no-deploy=traefik"
```

Explanation:

* We pass args that we want run on the servers with the `--k3s-server-arg` flag _(Notice we pass the arg right after it, but not in quotes)_
  * We're creating a new taint (we choose the name, it's not anything known to K8s)
  * The taint is also given an "effect" which tells K8s to not allow executing any pods
    on those nodes that do not have matching "tolerations" _and_ evict any pods
    that also don't have the right tolerations
* We're stopping k3d from deploying traefik because of [a bug](https://github.com/rancher/k3d/issues/430)
  that causes the traefik job to fail due to not having tolerations in the helm chart
  * We'll install it after

### Checking The Taint Was Applied

```bash
kubectl get nodes -o=custom-columns=NodeName:.metadata.name,TaintKey:.spec.taints[*].key,TaintValue:.spec.taints[*].value,TaintEffect:.spec.taints[*].effect
```

_Note: This might first show they're not ready, so run it again_

This should print out something like:

```bash
NodeName           TaintKey             TaintValue   TaintEffect
k3d-dev-server-0   CriticalAddonsOnly   true         NoExecute
k3d-dev-server-1   CriticalAddonsOnly   true         NoExecute
k3d-dev-server-2   CriticalAddonsOnly   true         NoExecute
```

Now if you try to install pods, they should fail to run on these nodes. Go ahead and try with `kubectl` to install something.

# Adding Worker Nodes

```bash
k3d node create worker --cluster dev --replicas 3
```

This will create multiple worker nodes as "agents". Run `kubectl get nodes` and you should see:

    NAME               STATUS   ROLES         AGE   VERSION
    k3d-dev-server-0   Ready    etcd,master   9m28s   v1.19.4+k3s1
    k3d-dev-server-1   Ready    etcd,master   9m9s    v1.19.4+k3s1
    k3d-dev-server-2   Ready    etcd,master   8m56s   v1.19.4+k3s1
    k3d-worker-0       Ready    <none>        23s     v1.19.4+k3s1
    k3d-worker-1       Ready    <none>        22s     v1.19.4+k3s1
    k3d-worker-2       Ready    <none>        21s     v1.19.4+k3s1

Now run `docker ps` and you should see the different nodes and notice the command run on the servers is `"/bin/k3s server --n…"` but on the worker nodes it's `"/bin/k3s agent"`

# Install Traefik

We have to install this after the worker nodes because Traefik's `values.yaml` only allows adding tolerations to the Traefik pods defined in the `Deployment` and _not_ to the `Service`. So it needs non-tainted nodes to run on.

```bash
# First add the helm repo
helm repo add traefik https://helm.traefik.io/traefik
helm repo update

# Now install with the toleration
#
# Note: The reason we use "Exists" and not "Equal" with the value as well is because Helm is unable
# to output quotes into the generated yaml. This results in an error on install where it says
# "value: true" was expcted to be a string (and not a boolean)
helm install traefik traefik/traefik --set tolerations[0].key=CriticalAddonsOnly --set tolerations[0].operator=Exists --set tolerations[0].effect=NoExecute
```

## Checking Traefik

```bash
# This will expose the dashboard for Traefik
kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000
```

You can now go to [http://127.0.0.1:9000/dashboard/](http://127.0.0.1:9000/dashboard/) in your browser and you should see the dashboard.

# Trying Out the Cluster

We're going to install the Nginx that Helm creates as a template chart. Then we'll look and see what pods everything ended up on.

## Create the Chart

```bash
# The name "testnginx" is free-form, we made it up
helm create testnginx
```

## Install Nginx

```bash
# Make sure you're NOT in the testnginx directory but in the parent
helm install testnginx testnginx/
```

### Complete "hooking" it up

The default helm chart doesn't actually expose everything properly for us to test so we have to do some traffic forwarding.

```bash
# Create a temporary env variable for the pod name and the container port (these come from the values in the chart)
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=testnginx,app.kubernetes.io/instance=testnginx" -o jsonpath="{.items[0].metadata.name}")
export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")

# Now start forwarding traffic
kubectl --namespace default port-forward $POD_NAME 8080:$CONTAINER_PORT
```

Now go to the address in your browser: [http://127.0.0.1:8080](http://127.0.0.1:8080)

## Verifying Node Scheduling

Let's see whether the tainting worked and put things on the right nodes:

```bash
kubectl get pods -o=custom-columns=NODE:.spec.nodeName,NAME:.metadata.name --all-namespaces --sort-by=.spec.nodeName
```

Command explanation:

* `-o` This allows us to override what it normally outputs from the `get pods` command which
  won't have most of the info we need
* `--all-namespaces` This will find all pods across all namespaces
* `--sort-by` This sorts the results by some property. In our case we use the node name

You should see something like:

```plaintext
NODE               NAME
k3d-dev-server-0   coredns-66c464876b-dnvrv
k3d-dev-server-0   local-path-provisioner-7ff9579c6-jzsv9
k3d-dev-server-0   metrics-server-7b4f8b595-wq7l8
k3d-dev-server-0   svclb-traefik-vchmb
k3d-dev-server-1   traefik-84846f6fcb-4kx6v
k3d-dev-server-1   svclb-traefik-9z4b2
k3d-dev-server-2   svclb-traefik-27wxr
k3d-worker-0       testnginx-6c774bcf58-xrq8l
k3d-worker-0       svclb-traefik-tf5pp
k3d-worker-1       svclb-traefik-bsrwv
k3d-worker-2       svclb-traefik-7wv4m
```

# Cleaning Up

## Delete the Cluster

```bash
k3d cluster delete dev
```

## Clean Up Docker

```bash
docker system prune -a
docker volume prune
```