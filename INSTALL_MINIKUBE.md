# Installing MiniKube

```bash
# Optional
sudo apt install curl

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```

## Install Ingress
```bash
minikube addons enable ingress
```

# Installing Kubectl
```bash
sudo snap install kubectl --classic
```

# Installing Docker
```bash
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# verify the key
sudo apt-key fingerprint 0EBFCD88

# setup the stable repo
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install docker engine
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Optional: Run without sudo https://docs.docker.com/engine/install/linux-postinstall/
sudo usermod -aG docker $USER

# Re-enter the session for the group change to work
su - $USER
```

# Helm

## Installing Helm
```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

# Installing Chartmuseum
```bash
docker run -d --rm -it -p 8282:8080 -v $(pwd)/charts:/charts -e DEBUG=true -e STORAGE=local -e STORAGE_LOCAL_ROOTDIR=/charts chartmuseum/chartmuseum:latest
```

# Connecting Helm to Chartmuseum with Push
```bash
helm plugin install https://github.com/chartmuseum/helm-push.git
helm repo add local-dev http://localhost:8282
```

# If using VSCode Kubernetes Extension
VS Code's Kubernetes extension doesn't work with kubectl installed
via snap. You'll get failures, unless you enable a setting.
```javascript
/*
 * Since we installed kubectl via snap
 * 1. Install latest kubernetes plugin
 * 2. Go to File->preferences->settings->extensions->Kubernetes
 * 3. Click the underlined link "edit settings.json"
 * 4. Underneath "vs-kubernetes", add in:
 */
"vs-kubernetes.enable-snap-flag": true,
```